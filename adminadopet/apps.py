from django.apps import AppConfig


class AdminadopetConfig(AppConfig):
    name = 'adminadopet'
