# AdoPet

AdoPet adalah aplikasi berbasis website untuk menyatukan para pecinta hewan dan orang-orang yang peduli terhadap kesejahteraan hewan dari seluruh kalangan untuk bekerja sama mensejahterakan kehidupan hewan-hewan tersebut.

develop : [![pipeline status](https://gitlab.com/m.yoga.mahendra/adopet/badges/develop-2/pipeline.svg)](https://gitlab.com/m.yoga.mahendra/adopet/-/commits/develop-2) 

## Rekayasa Perangkat Lunak 

#### Anggota Kelompok : 
- Gilbert Stefano Wijaya (1806205584) 
- Muhamad Yoga Mahendra (1806205256)
- Nabilah Adani Nurulizzah (1706979386) 
- Zafira Binta Feliandra (1806205155)

#### Pengerjaan Fitur : 

##### Steven : 
- Melakukan Donasi 
- Forum
##### Yoga : 
- Search 
- Chat 
##### Nabilah : 
- Artikel 
- Testimoni 
- Pengadopsian Hewan 
##### Zafira : 
- Open Adopt 
- Open Donasi 

#### Link Heroku App : https://ado-pet.herokuapp.com/